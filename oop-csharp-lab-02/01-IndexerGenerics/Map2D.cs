﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IndexerGenerics
{
    public class Map2D<TK1, TK2, TV> : IMap2D<TK1, TK2, TV>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        private readonly IDictionary<Tuple<TK1, TK2>, TV> _map = new Dictionary<Tuple<TK1, TK2>, TV>();

        public Map2D()
        { }
        
        public void Fill(IEnumerable<TK1> keys1, IEnumerable<TK2> keys2, Func<TK1, TK2, TV> generator)
        {
            _map.Clear();
            foreach (var k1 in keys1)
            {
                foreach (var k2 in keys2)
                {
                    _map.Add(new Tuple<TK1, TK2>(k1, k2), generator.Invoke(k1, k2));
                }
            }
        }

        public TV this[TK1 key1, TK2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */
            get { return _map[new Tuple<TK1, TK2>(key1, key2)]; }

            set { _map[new Tuple<TK1, TK2>(key1, key2)] = value; }
        }
        
        public IList<Tuple<TK2, TV>> GetRow(TK1 key1)
        {
            IList<Tuple<TK2, TV>> temp = new List<Tuple<TK2, TV>>();
            foreach (var kvPair in _map)
            {
                if (kvPair.Key.Item1.Equals(key1))
                {
                    temp.Add(new Tuple<TK2, TV>(kvPair.Key.Item2, kvPair.Value));
                }
            }
            return temp;
        }

        public IList<Tuple<TK1, TV>> GetColumn(TK2 key2)
        {
            IList<Tuple<TK1, TV>> temp = new List<Tuple<TK1, TV>>();
            foreach (var kvPair in _map)
            {
                if (kvPair.Key.Item2.Equals(key2))
                {
                    temp.Add(new Tuple<TK1, TV>(kvPair.Key.Item1, kvPair.Value));
                }
            }
            return temp;
        }

        public IList<Tuple<TK1, TK2, TV>> GetElements()
        {
            IList<Tuple<TK1, TK2, TV>> temp = new List<Tuple<TK1, TK2, TV>>();
            foreach (var kvPair in _map)
            {
                temp.Add(new Tuple<TK1, TK2, TV>(kvPair.Key.Item1, kvPair.Key.Item2, kvPair.Value));
            }

            return temp;
        }

        public int NumberOfElements
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return _map.ToString();
        }

        public bool Equals(IMap2D<TK1, TK2, TV> other)
        {
            return other.GetElements().Equals(GetElements());
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IMap2D<TK1, TK2, TV>))
            {
                return false;
            }
            return Equals((IMap2D<TK1, TK2, TV>)obj);
        }

        public override int GetHashCode()
        {
            return _map.GetHashCode();
        }
    }
}
